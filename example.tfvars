# Variables for the CA
ca_cname = "Laodicea"
ca_organization = "Laodicea, Inc"
ca_validity_period_hours = 17520

# Variables for cert signed by the CA
tls_cname = "local.domain"
tls_altnames = [
  "*.local.domain",
]
tls_organization = "Kubernetes, Inc"
tls_validity_period_hours = 17520
