variable "ca_cname" {}
variable "ca_organization" {}
variable "ca_validity_period_hours" {}

variable "tls_cname" {}
variable "tls_altnames" {}
variable "tls_organization" {}
variable "tls_validity_period_hours" {}
